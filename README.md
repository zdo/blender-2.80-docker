# Blender 2.80 Beta inside a docker image

Run `./docker-build` for building the docker image.

Then you can use the link `registry.gitlab.com/zdo/blender-2.80-docker/blender/2.80:latest`.

Blender executable file is located at `/blender/blender`. This path is also written into `$BLENDER_EXE`.
