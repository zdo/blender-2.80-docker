FROM plumbee/nvidia-virtualgl:2.5.2

WORKDIR /blender

RUN apt-get update && \
    apt-get install -y --no-install-recommends wget bzip2 libxi6 curl

RUN echo "https://builder.blender.org$(curl --silent 'https://builder.blender.org/download/' | grep '/download/blender-2.80-[^\/]*x86_64.tar.bz2' -o)" > /tmp/blender_link && \
    wget --no-check-certificate "$(cat /tmp/blender_link)" -O blender.tar.bz2 && \
    tar xjf blender.tar.bz2 --strip-components=1 && \
    rm blender.tar.bz2 /tmp/blender_link

ENV BLENDER_EXE="/blender/blender"
